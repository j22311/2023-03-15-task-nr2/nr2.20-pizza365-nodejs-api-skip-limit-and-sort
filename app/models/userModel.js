//khai bao Mongoose
const mongoose = require('mongoose');

//khai bao thu vien Schema trong MOngoose
const Schema = mongoose.Schema;

//khai bao cac colection bang Schema
const userSchema = new Schema({
    id: mongoose.Types.ObjectId,
    fullName: {
        type: String,
        require: true
    },
    email: {
        type: String,
        require: true,
        unique: true
    },
    address: {
        type: String,
        require: true,
        
    },
   phone: {
        type: String,
        require: true,
        unique: true
    },
    order:[
        {
            type: mongoose.Types.ObjectId,
            ref: "order"
        }
    ]
});

module.exports = mongoose.model("user",userSchema);