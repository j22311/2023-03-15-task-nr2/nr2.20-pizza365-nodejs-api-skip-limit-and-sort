//khai báo thư viện moongoose
const mongoose = require('mongoose');

//khai báo schema để tạo schema
const Schema = mongoose.Schema;

// khai báo voucherSchema tạo các collection 
const voucherSchema = new Schema({
    //_id: mongoose.Types.ObjectId,
    maVoucher: {
        type: String,
        unique: true,
        require: true
    },
    phanTramGiamGia: {
        type: Number,
        require: true
    },
    ghiChu: {
        type: String,
        require: false
    }
})

module.exports = mongoose.model("voucher",voucherSchema);