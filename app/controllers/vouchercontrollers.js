// import thư viện mongoose 
const mongoose = require('mongoose');

// import model voucher trong thư mục models 
const voucherModel = require('../models/voucherModel');

//-----------------------lấy hết dữ liệu voucher
const getAllVoucher = (request,response) => {
    // b1: thu thập dữ liệu
    // b2: validate dữ liệu 
    // b3: gọi model tạo dữ liệu 
    voucherModel.find()
   .then((data) => {
      response.status(201).json({
         message: 'Successful to show all vouchers',
         drink: data
      })
   })
   .catch((error)=> {
      response.status(500).json({
         message: `internal sever ${error}`
      })
    })
   
 }
 //lấy một dữ liệu voucher bằng id
//-------------------------get a voucher by ID 
const getAVoucherById = (request,response) => {
    //b1: thu thập dữ liệu
    const voucherId = request.params.voucherId;
 
    //b2: Validate dữ liệu
    if(!mongoose.Types.ObjectId.isValid(voucherId)){
       return response.status(400).json({
          status: 'Bad Request',
          message: `${voucherId} ko hợp lệ`
       })
    }
    //bb3: gọi model tìm dữ liêu
    voucherModel.findById(voucherId)
       .then((data) => {
          response.status(201).json({
             message: 'successful to find one voucher ',
             voucher: data
          })
       })
       .catch((error) => {
          response.status(500).json({
             message: `internal sever ${error}`
          })
       })
 }
 //---------------------------insert a drink
 const insertAVoucher = (request, response) => {
   
   // b1: thu thập dữ liệu
   const body = request.body;
   console.log(body);
    // b2: validate dữ liệu 
   if(!body.maVoucher){
      return response.status(400).json({
         status: 'Bad request',
         message: 'maVoucher ko hop le'
      })
   }
//    if(!body.ghiChu){
//       return response.status(400).json({
//          status: 'Bad request',
//          message: 'ghiChu ko hop le'
//       })
//    }

   if(isNaN(body.phanTramGiamGia) || body.phanTramGiamGia < 0){
      return response.status(400).json({
         status: 'bad request',
         message: 'phanTramGiamGia ko hop le'
      })
   }
    // b3: gọi model tạo dữ liệu 
    const newVoucher = new voucherModel( {
      //  _id: mongoose.Types.ObjectId,
      maVoucher : body.maVoucher,
      phanTramGiamGia : body.phanTramGiamGia,
      ghiChu: body.ghiChu
    })
    console.log(newVoucher);
   
    voucherModel.create(newVoucher)
    .then((data) => {
         response.status(201).json({
            message: 'Successful to create new voucher',
            voucher: data
         })
    })
    .catch((error)=> {
      response.status(500).json({
         message: `internal sever ${error}`
      })
    })
 }
//-----------------update a drinks----------------------------
const updateAVoucherById = (request,response) => {
    //b1: thu thập dữ liệu
    const voucherId = request.params.voucherId;
    const body = request.body;
    //b2: validate dữ liệu
    if(!mongoose.Types.ObjectId.isValid(voucherId)){
       return response.status(400).json({
          status: 'Bad request',
          message: `${voucherId} ko hợp lệ`
       })
    }
    if(!body.maVoucher){
        return response.status(400).json({
           status: 'Bad request',
           message: 'maVoucher ko hop le'
        })
     }
  //    if(!body.ghiChu){
  //       return response.status(400).json({
  //          status: 'Bad request',
  //          message: 'ghiChu ko hop le'
  //       })
  //    }
  
     if(isNaN(body.phanTramGiamGia) || body.phanTramGiamGia < 0){
        return response.status(400).json({
           status: 'bad request',
           message: 'phanTramGiamGia ko hop le'
        })
     }
 
     // b3: gọi model để update dữ liệu
     const newVoucherUpdate = {
        //  _id: mongoose.Types.ObjectId,
        maVoucher : body.maVoucher,
        phanTramGiamGia : body.phanTramGiamGia,
        ghiChu: body.ghiChu
      };
      console.log(voucherId);
    voucherModel.findByIdAndUpdate(voucherId,newVoucherUpdate)
       .then((data) => {
          response.status(201).json({
             message: 'Successful to update a vouchers',
             voucher: data
          })
       })
       .catch((error) => {
          response.status(500).json({
          message: `internal sever ${error}`
          })
       })
 
 }

 //--------------delete a voucherbyID----------------------
const deleteAVoucherById = ((request,response) => {
    //b1: thu thập dữ liệu
    const voucherId = request.params.voucherId;
    //b2: kiểm tra dữ liệu
    if(!mongoose.Types.ObjectId.isValid(voucherId)){
       return response.status(400).json({
          status: 'Bad request',
          message: `${voucherId} ko hợp lệ`
       })
    }
    //b3: gọi model xóa dữ liệu
    voucherModel.findByIdAndDelete(voucherId)
    .then((data) => {
       response.status(201).json({
          message: 'Successful to delete a voucher ',
          voucher: data
       })
    })
    .catch((error) => {
       response.status(500).json({
       message: `internal sever ${error}`
       })
    })
 })

 module.exports = {
    getAllVoucher,
    getAVoucherById,
    insertAVoucher,
    updateAVoucherById,
    deleteAVoucherById
 }