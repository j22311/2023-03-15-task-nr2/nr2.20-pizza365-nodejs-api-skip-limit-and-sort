const getAllUsersMiddleware = (req,res,next) => {
    console.log("get All Users (middleware)");
    next();
}

const getOneUserByIdMiddleware = (req,res,next) => {
    console.log("get One User (middleware)");
    next();
}

const postOneUserMiddleware = (req,res,next) => {
    console.log("post One User (middleware)");
    next();
}

const putOneUserByIdMiddleware = (req,res,next) => {
    console.log("put One User (middleware)");
    next();
}

const deleteOneUserByIdMiddleware = (req,res,next) => {
    console.log("get one User (middleware)");
    next();
}

module.exports = {
    deleteOneUserByIdMiddleware,
    putOneUserByIdMiddleware,
    postOneUserMiddleware,
    getOneUserByIdMiddleware,
    getAllUsersMiddleware
}

