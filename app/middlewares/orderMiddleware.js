const getAllOrdersMiddleware = (req,res,next) => {
    console.log("get All Orders (middleware)");
    next();
}

const getOneOrderByIdMiddleware = (req,res,next) => {
    console.log("get One Order (middleware)");
    next();
}

const postOneOrderMiddleware = (req,res,next) => {
    console.log("post One Order (middleware)");
    next();
}

const putOneOrderByIdMiddleware = (req,res,next) => {
    console.log("put One Order (middleware)");
    next();
}

const deleteOneOrderByIdMiddleware = (req,res,next) => {
    console.log("get one Order (middleware)");
    next();
}

module.exports = {
    deleteOneOrderByIdMiddleware,
    putOneOrderByIdMiddleware,
    postOneOrderMiddleware,
    getOneOrderByIdMiddleware,
    getAllOrdersMiddleware
}

